package ast;

import java.util.List;

import environment.Environment;

/**
 * Class that holds all of the procedures in the program as well as the final
 * statement that is to be executed
 * Also, when executed, adds all of the procedures to the global environment, where they
 * are stored
 * Can be compiled to produce MIPS code that can be used in QtSpim
 * @author AyushAlag
 * @version November 17, 2017
 *
 */
public class Program extends Statement
{
	private Statement endStatement;
	private List<ProcedureDeclaration> pd;
	private List<String> variables;
	
	/**
	 * Sets the instance variables to the parameters
	 * @param p is the list of procedure declarations
	 * @param es is the final statement to be executed
	 */
	public Program(List<String> v, List<ProcedureDeclaration> p, Statement es)
	{
		pd = p;
		endStatement = es;
		variables = v;
	}
	
	/**
	 * When this is called, it adds all of the procedures to the global environment
	 * and then executes the final statement
	 * @param env the environment in which this is called
	 */
	public void exec(Environment env)
	{
		for (ProcedureDeclaration p: pd)
		{
			env.setProcedure(p);
		}
		endStatement.exec(env);
	}
	
	/**
	 * Central compile program, which first sets up the data section with
	 * variables, generates the procedure calls, 
	 * establishes the global main, compiles the statement in the program,
	 * and then ends the program
	 * $v0 is like the master register (where the values are stored and operated from)
	 */
	public void compile(Emitter e)
	{
		e.emit(".data");
		e.emit("newLine: .asciiz \"\\n\"");
		for (String v: variables)
		{
				e.emit("var"+ v + ": .word "+0);
		}
		e.emit(".text");
		e.emit(".globl main");
		e.emit("\n");
		for (ProcedureDeclaration p: pd)
		{
			p.compile(e);
		}
		e.emit("main: ");
		endStatement.compile(e);
		e.emit("li $v0, 10");
		e.emit("syscall");
		e.close();
	}
}
