package ast;

import environment.Environment;

/**
 * Used if an else-if statement is found in the text
 * Checks whether the first condition is true; if it is, then it evaluates the first statement;
 * else, it evaluates the last statement.
 * @author AyushAlag
 * @version 10/12/17
 *
 */
public class ElseIf extends Statement
{
	private Condition cond;
	private Statement stmt;
	private Statement elsestmt;
	
	/**
	 * Sets the instance variables to the parameters
	 * @param c the condition
	 * @param s the first statement (under the if)
	 * @param es the else statement
	 */
	public ElseIf(Condition c, Statement s, Statement es)
	{
		cond = c;
		stmt = s;
		elsestmt = es;
	}
	
	/**
	 * Checks whether the first condition is true; if it is, then it executes the first statement;
	 * else, it executes the last statement.
	 */
	public void exec(Environment env)
	{
		if (cond.eval(env))
			stmt.exec(env);
		else
		{
			elsestmt.exec(env);
		}
	}
}