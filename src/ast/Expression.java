package ast;

import environment.Environment;

/**
 * Abstract framework for subclasses of Expression
 * Each expression object/class can evaluate itself using an evironment
 * It can also compile itself in MIPS code
 * @author AyushAlag
 * @version November 17, 2017
 *
 */
public abstract class Expression 
{
	public abstract int eval(Environment env);
	
	/**
	 * Provides framework to write code to compile the expression
	 * @param e the Emitter that is used to transfer the Mips Code written here
	 * to a new document that can be used in qtSpim
	 */
	public void compile(Emitter e)
	   {
	     throw new RuntimeException("Implement me!!!!!");
	}
}
