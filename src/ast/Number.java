package ast;

import environment.Environment;

/**
 * Is a number, containing an int value
 * Returns the int value it contains when it is evaluated
 * Can also compile the number
 * @author AyushAlag
 * @version November 17, 2017
 */
public class Number extends Expression 
{
	private int value;
	public Number (int v)
	{
		value = v;
	}
	/**
	 * Returns the int value it contains
	 */
	public int eval(Environment env)
	{
		return value;
	}
	
	/**
	 * Transfers the integer to $v0, which is where the values are being stored
	 * and operated from (like a master register)
	 */
	public void compile (Emitter e)
	{
		e.emit("li $v0, " + value);
	}
	
}
