package ast;

import java.util.List;

import environment.Environment;

/**
 * Framework for a block of statements; each is stored in a list,
 * and when the method is called to execute the block, each statement is executed
 * @author AyushAlag
 * @version November 17, 2017
 */
public class Block extends Statement
{
	private List<Statement> stmts;
	public Block (List<Statement> s)
	{
		stmts = s;
	}
	
	/**
	 * Executes each of the statements in the block
	 */
	public void exec(Environment env)
	{
		for (int i = 0; i<stmts.size(); i++)
		{
			stmts.get(i).exec(env);
		}
	}
	
	/**
	 * Compiles the block of statements by compiling each statement one after 
	 * the other
	 */
	public void compile(Emitter e)
	{
		for (int i = 0; i<stmts.size(); i++)
		{
			stmts.get(i).compile(e);
		}
	}
}