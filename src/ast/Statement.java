package ast;

import environment.Environment;

/**
 * Abstract framework for Statement classes
 * All statements can execute themselves using an environment
 * All statements can compile themselves using an emitter
 * @author AyushAlag
 * @version November 17, 2017
 *
 */
public abstract class Statement 
{
	public abstract void exec(Environment env);
	
	/**
	 * Provides framework to write code to compile the statement
	 * @param e the Emitter that is used to transfer the Mips Code written here
	 * to a new document that can be used in qtSpim
	 */
	public void compile(Emitter e)
	   {
	     throw new RuntimeException("Implement me!!!!!");
	}
}
