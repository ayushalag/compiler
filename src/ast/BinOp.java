package ast;

import environment.Environment;

/**
 * Is an binary operator, containing two expressions and an operator
 * Evaluates the two expressions using the operator
 * Also compiles the binary operator to mips code
 * @author AyushAlag
 * @version November 17, 2017
 */
public class BinOp extends Expression
{
	private String op;
	private Expression exp1;
	private Expression exp2;
	
	/**
	 * Sets the instance variables to the parameters
	 * @param o the operand
	 * @param e1 the first expression (being operated on)
	 * @param e2 the second expression (operating on the first)
	 */
	public BinOp (String o, Expression e1, Expression e2)
	{
		op = o;
		exp1 = e1;
		exp2 = e2;
	}
	
	/**
	 * After checking what type of operand it is, this method
	 * evaluates the two expressions accordingly, returning the final value
	 */
	public int eval(Environment env)
	{
		if (op.equals("+"))
		{
			return exp1.eval(env) + exp2.eval(env);
		}
		else if (op.equals("-"))
		{
			return exp1.eval(env) - exp2.eval(env);
		}
		else if (op.equals("*"))
		{
			return exp1.eval(env) * exp2.eval(env);
		}
		else if (op.equals("/"))
		{
			return exp1.eval(env) / exp2.eval(env);
		}
		else
		{
			//mod operator
			return exp1.eval(env) % exp2.eval(env);
		}
	}
	
	/**
	 * Evaluates the two expressions using the given operand
	 * Uses the stack and to hold multiple registers and values, pushing the
	 * stack pointer up and down
	 */
	public void compile(Emitter e)
	{
		exp1.compile(e);
		e.emitPush("$v0");
		exp2.compile(e);
		e.emitPop("$t0");
		if (op.equals("+"))
		{
			e.emit("addu $v0, $t0, $v0");
		}
		else if (op.equals("-"))
		{
			e.emit("subu $v0, $t0, $v0");
		}
		else if (op.equals("*"))
		{
			e.emit("mult $v0, $t0");
			e.emit("mflo $v0");
		}
		else if (op.equals("/"))
		{
			e.emit("div $t0, $v0");
			e.emit("mflo $v0");
		}
		else
		{
			//mod operator
			e.emit("div $v0, $t0");
			e.emit("mfhi $v0");
		}
	}
}
