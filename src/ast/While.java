package ast;

import environment.Environment;

/**
 * Is used to handle while loops
 * Continually executes the statement while the condition is true
 * Can also be compiled to MIPS code
 * @author AyushAlag
 * @version November 17, 2017
 */
public class While extends Statement
{
	private Condition cond;
	private Statement stmt;
	
	public While(Condition c, Statement s)
	{
		cond = c;
		stmt = s;
	}
	
	/**
	 * When called to execute, the method checks to see that the condition is true and 
	 * then evaluates the statement. The statement is repeatedly called while the condition 
	 * is true
	 */
	public void exec(Environment env)
	{
		while(cond.eval(env))
		{
			stmt.exec(env);
		}
	}
	
	/**
	 * The while statement compiles by creating a loop field in which it first 
	 * compiles the condition, which breaks the loop and goes to the end label when
	 * the condition is false. It then compiles the statement inside the loop and
	 * jumps back to the beginning of the loop
	 * Takes care of repetitive labels by using the nextLabelId method in the emitter
	 * class
	 */
	public void compile(Emitter e)
	{
		String looplabel = "loop" + e.nextLabelID();
		String endlabel = "endloop" + e.nextLabelID();
		e.emit(looplabel + ": ");
		cond.compile(e, endlabel);
		stmt.compile(e);
		e.emit("j " + looplabel);
		e.emit(endlabel + ": ");
		
	}
}
