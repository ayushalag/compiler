package ast;

import environment.Environment;
/**
 * Is an assignment operation, containing a variable name and an expression value
 * Creates a new variable in the environment with the given variable name and value
 * Can be compiled to store the variable in memory
 * Updated for local variables in procedure calls
 * @author AyushAlag
 * @version December 22, 2017
 */
public class Assignment extends Statement
{
	private String var;
	private Expression exp;
	/**
	 * Assigns the parameters to the instance variables
	 * @param v the inputted name of the variable
	 * @param e the inputted Expression object
	 */
	public Assignment (String v, Expression e)
	{
		var = v;
		exp = e;
	}
	
	/**
	 * Executes the assignment operator by evaluating the expression it contains
	 * and then creating a new variable with the given name and value of the expression
	 */
	public void exec(Environment env)
	{
		Integer value = exp.eval(env);
		env.setVariable(var, value);
	}
	
	/**
	 * Stores the variable into memory if it is not a local variable; otherwise,
	 * it gets the offset and then stores the variable into that stack address
	 */
	public void compile(Emitter e)
	{
		exp.compile(e);
		if (!e.isLocalVariable(var))
			e.emit("sw $v0, var" + var);
		else
		{
			int off = e.getOffset(var);
			e.emit("sw $v0, " + off + "($sp)");
		}
	}
}
