package ast;

import environment.Environment;

/**
 * Framework for the condition function, which can evaluate (using a relative operator)
 * if a condition is true (using both expressions and evaluating them with the relative operator)
 * @author AyushAlag
 * @version November 17, 2017
 *
 */
public class Condition
{
	private Expression exp1;
	private String relop;
	private Expression exp2;
	
	public Condition(Expression e1, String r, Expression e2)
	{
		exp1 = e1;
		exp2 = e2;
		relop = r;
	}
	
	/**
	 * Evaluates whether the condition is true (according to the type of 
	 * relative operator) and returns the boolean result
	 * @param env the Environment that holds the instance variables
	 * @return the boolean result of the condition (true/false)
	 */
	public boolean eval(Environment env)
	{
		if (relop.equals(">"))
			return (exp1.eval(env) > exp2.eval(env));
		else if (relop.equals("<"))
			return (exp1.eval(env) < exp2.eval(env));
		else if (relop.equals("="))
			return (exp1.eval(env) == exp2.eval(env));
		else if (relop.equals(">="))
			return (exp1.eval(env) >= exp2.eval(env));
		else if (relop.equals("<="))
			return (exp1.eval(env) <= exp2.eval(env));
		else
		{
			//relop is <>
			return (exp1.eval(env)!=exp2.eval(env));
		}
	}
	
	/**
	 * Produces code to evaluate the condition, jumping to the paramater "label"
	 * if the condition evaluates to false
	 * @param e the emitter being used to push/pop onto stack and also to send output
	 * to an output file
	 * @param label the label to which the code should jump if the condition evaluated
	 * is false
	 */
	public void compile(Emitter e, String label)
	{
		exp1.compile(e);
		e.emitPush("$v0");
		exp2.compile(e);
		e.emitPop("$t0");
		if (relop.equals(">"))
			e.emit("ble $t0, $v0, " + label);
		else if (relop.equals("<"))
			e.emit("bge $t0, $v0, " + label);
		else if (relop.equals("="))
			e.emit("bne $t0, $v0, " + label);
		else if (relop.equals(">="))
			e.emit("blt $t0, $v0, " + label);
		else if (relop.equals("<="))
			e.emit("bgt $t0, $v0, " + label);
		else
		{
			//relop is <>
			e.emit("beq $t0, $v0, " + label);
		}
	}
}
