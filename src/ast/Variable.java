package ast;

import environment.Environment;

/**
 * Evaluates a variable, using the environment to find the value of the 
 * given variable and returning that value
 * Can also compile the variable, taking its value form memory into a temporary variable
 * Updated to retrieve the values of local variables from procedure calls
 * @author AyushAlag
 * @version December 22, 2017
 *
 */
public class Variable extends Expression
{
	private String name;
	public Variable (String n)
	{
		name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	/**
	 * Evaluates a variable, using the environment to find the value of the 
	 * given variable and returning that value
	 */
	public int eval(Environment env)
	{
		return env.getVariable(name);
	}
	
	/**
	 * Code to retrieve the value of the variable from memory and move it to 
	 * $v0, if it is not a local variable; otherwise,
	 * it finds the offset and loads the value from that location in the stack into
	 * $v0
	 */
	public void compile(Emitter e)
	{
		if (!e.isLocalVariable(name))
		{
			e.emit("lw $t0, var" + name);
			e.emit("move $v0, $t0");
		}
		else
		{
			int offset = e.getOffset(name);
			e.emit("lw $v0, " + offset + "($sp)");
		}
	}
}
