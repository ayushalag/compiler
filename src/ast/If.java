package ast;

import environment.Environment;

/**
 * Used to handle if statements
 * Checks if the condition is true; if it is, then the statement is executed
 * Can also compile the if statements
 * @author AyushAlag
 * @version November 17, 2017
 *
 */
public class If extends Statement
{
	private Condition cond;
	private Statement stmt;
	
	public If(Condition c, Statement s)
	{
		cond = c;
		stmt = s;
	}
	
	/**
	 * Checks if the condition is true; if it is, then the statement is executed
	 */
	public void exec(Environment env)
	{
		if (cond.eval(env))
			stmt.exec(env);
	}
	
	/**
	 * Code to evaluate the condition; if it evaluates to false, it jumps to the end label,
	 * skipping the "then" statement and going to the else that is the rest of the code
	 */
	public void compile(Emitter e)
	{
		String label = "endif" + e.nextLabelID();
		cond.compile(e, label);
		stmt.compile(e);
		e.emit(label + ": ");
	}
}
