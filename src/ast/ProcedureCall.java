package ast;

import java.util.List;

import environment.Environment;

/**
 * Framework for when a procedure is called; holds the name of the procedure
 * and the values (Expressions) of the inputted parameters. When evaluated,
 * the formal and actual parameters are equated and stored as variables and
 * the statement associated with the procedure is called
 * Takes care of global and local environments
 * 
 * Also updated to successfully compile procedure calls to MIPS
 * @author AyushAlag
 * @version December 22, 2017
 *
 */
public class ProcedureCall extends Expression
{
	private String id;
	private List<Expression> parmVals;
	
	/**
	 * Sets the instance variables to the parameters
	 * @param i is the name of the procedure
	 * @param ps is the list of actual parameters of the procedure that are
	 * passed in by the caller of the method
	 */
	public ProcedureCall(String i, List<Expression> ps)
	{
		id = i;
		parmVals = ps;
	}
	
	/**
	 * Equates the formal and actual parameters and stores
	 * these as variables with their formal value as the name and their associated
	 * expression as the value. This is stored in a local (procedure) environment
	 * The procedure statement is then executed
	 * Return types are also taken care of (the variable associated with the method name
	 * is returned), and this value is initially set to 0
	 * @return int the value of the return variable (same as procedure name)
	 * @param env is the environment in which this is called
	 */
	public int eval(Environment env)
	{
		Environment prEnv = new Environment(env);
		prEnv.declareVariable(id, 0);
		List<String> paramNames = prEnv.getProcedure(id).getParams();
		for (int i = 0; i<parmVals.size(); i++)
		{
			int val = parmVals.get(i).eval(env);
			prEnv.declareVariable(paramNames.get(i), val);
		}
		prEnv.getProcedure(id).getStatement().exec(prEnv);
		return prEnv.getVariable(id);
	}
	
	/**
	 * Uses the emitter to compile the procedure call to MIPS code,
	 * pushing the return address and parameters (in that order) onto the stack
	 * It then calls the procedure (using the jal function), and then pops off the
	 * variables into the t0 (temporary register) and pops off the return address
	 */
	public void compile(Emitter e)
	{
		e.emitPush("$ra");
		for (int i = 0; i<parmVals.size(); i++)
		{
			parmVals.get(i).compile(e);
			e.emitPush("$v0");
		}
		e.emit("jal proc" + id);
		for (int i = 0; i<parmVals.size(); i++)
		{
			e.emitPop("$t0");
		}
		e.emitPop("$ra");
	}
}
