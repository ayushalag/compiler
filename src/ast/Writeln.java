package ast;

import environment.Environment;

/**
 * Handles the WRITELN block by printing out the value of the 
 * expression contained inside the block
 * Can also be compiled to MIPS code
 * @author AyushAlag
 * @version November 17, 2017
 */
public class Writeln extends Statement 
{
	private Expression exp;
	public Writeln(Expression e)
	{
		exp = e;
	}
	
	/**
	 * Prints out the value of the expression contained inside the block
	 */
	public void exec(Environment env)
	{
		System.out.println(exp.eval(env));
	}
	
	/**
	 * The WRITELN statement compiles by printing out the compiled expression
	 */
	public void compile(Emitter e)
	{
		exp.compile(e);
		e.emit("move $a0, $v0");
		e.emit("li $v0, 1");
		e.emit("syscall");
		e.emit("la $a0, newLine");
		e.emit("li $v0, 4");
		e.emit("syscall");
	}
}
