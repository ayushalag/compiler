package ast;

import environment.Environment;

public class For extends Statement
{
	private String currName;
	private Expression startValue;
	private Expression endValue;
	private Statement stmt;
	
	public For(String cuName, Expression cVal, Expression e, Statement st)
	{
		currName = cuName;
		startValue = cVal;
		endValue = e;
		stmt = st;
	}
	
	public void exec(Environment env)
	{
		//boolean goUp = true;
		env.setVariable(currName, startValue.eval(env));
		while (env.getVariable(currName) < endValue.eval(env))
		{
			stmt.exec(env);
			env.setVariable(currName, env.getVariable(currName)+1);			
		}
		
//		if (endValue.eval(env) > startValue.eval(env))
//			goUp = true;
/*		if (endValue.eval(env) <startValue.eval(env))
			goUp = false;
		int i = 0;
		while (isTrue(env, goUp))
		{
			stmt.exec(env);
			i++;
			if (goUp)
			{
				env.setVariable(currName, startValue.eval(env)+i);
			}
			else
			{
				env.setVariable(currName, startValue.eval(env)-i);
			}
		}*/
	}
	
	public boolean isTrue(Environment env, boolean b)
	{
		if (b)
		{
			return (startValue.eval(env) < endValue.eval(env)) && (startValue.eval(env)!=endValue.eval(env));
		}
		else
			return (startValue.eval(env)>endValue.eval(env));
	}
}
