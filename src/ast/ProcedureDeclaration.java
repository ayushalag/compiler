package ast;

import java.util.List;

/**
 * Is the framework for the declaration of a procedure, holding the name
 * of the procedure, the statement that goes along with it, and all of the parameters
 * of the procedure
 * 
 * Updated to compile procedure declarations that occur at the start of the code (after
 * global variable declarations); also takes care of local variables inside the procedure
 * and returning values (via the variable name of the procedure)
 * @author AyushAlag
 * @version December 22, 2017
 *
 */
public class ProcedureDeclaration
{
	private String id;
	private Statement stmt;
	private List<String> parms;
	private List<String> vars;
	
	/**
	 * Sets the instance variables to the parameters
	 * @param i is the name of the string
	 * @param s is the statement associated with the procedure
	 * @param ps is the list of parameter names
	 * @param v is the list of local variables in the method
	 */
	public ProcedureDeclaration(String i, Statement s, List<String> ps, List<String> v)
	{
		id = i;
		stmt = s;
		parms = ps;
		vars = v;
	}
	
	/**
	 * Getter for the statement
	 * @return the Statement that is the statement associated with the procedure
	 */
	public Statement getStatement()
	{
		return stmt;
	}
	
	/**
	 * Getter for the name of the procedure
	 * @return a String that is the name of the procedure
	 */
	public String getId()
	{
		return id;
	}
	
	/**
	 * Getter for the list of parameter names of the procedure
	 * @return the List of Strings that are the names of the parameters of the procedure
	 */
	public List<String> getParams()
	{
		return parms;
	}
	
	/**
	 * Getter for the list of variable names of the procedure
	 * @return the List of Strings that are the names of the local variables of the procedure
	 */
	public List<String> getVars()
	{
		return vars;
	}
	
	/**
	 * Code to compile the Procedure declaration
	 * First declares the beginning of the procedure declaration and then
	 * Pushes 0s onto the stack (representing the variables and the return address)
	 * It then sets the Procedure context (which gives the emitter this procedure declaration
	 * and also measures the number of excessStackvariables used during the method, which helps
	 * determine the correct offset)
	 * The statement inserted in the procedure is then compiled and then pops off the variables
	 * and the return value (with the method name) into $v0. The return address is then extracted
	 * into $ra and the procedure context is cleared (with the emitter's procedure declaration variable
	 * being set to null and its excessStackHeight being set to 0)
	 * @param e Emitter that emits the compiled code to a MIPS file
	 */
	public void compile(Emitter e)
	{
		e.emit("proc" + id + ":");
		for (int i = 0; i<vars.size()+1; i++) //puts 0s representing variables + one more for the 
		{									  // method name variable which holds the return value
			e.emit("li $v0, 0");
			e.emitPush("$v0");
		}
		
		e.setProcedureContext(this);
		stmt.compile(e);
		for (int i = 0; i<vars.size()+1; i++)
		{
			e.emit("lw $v0, "+ 4*i +"($sp)"); //returns the proc call name as the return value in v0
			e.emit("addu $sp, $sp, " + 4);
		}
		e.emit("jr $ra");
		e.clearProcedureContext();
	}
}
