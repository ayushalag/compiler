package ast;

import java.io.*;
import java.util.List;

/**
 * Can emit the inputted text to an external file
 * Also modified to push and pop onto the stack
 * 
 * Updated to take care of procedure context and monitoring the stack height
 * @author AyushAlag
 * @author Anu Datar
 * @version December 22, 2017
 *
 */
public class Emitter
{
	private PrintWriter out;
	private int labelId;
	ProcedureDeclaration currProc;
	private int excessStackHeight;

	//creates an emitter for writing to a new file with given name
	public Emitter(String outputFileName)
	{
		labelId = 0;
		currProc = null;
		try
		{
			out = new PrintWriter(new FileWriter(outputFileName), true);
		}
		catch(IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Sets the emitter's procedureDeclaration variable to the parameter (used to check whether
	 * variables are local or not as well as computing the offset if they are local variables)
	 * Initializes the excessStack Height to 0
	 * @param proc the ProcedureDeclaration being set in the emitter
	 */
	public void setProcedureContext(ProcedureDeclaration proc)
	{
		currProc = proc;
		excessStackHeight = 0;
	}
	
	/**
	 * Removes the preexisting procedureDeclaration object from 
	 * the instance variable (sets it to null)
	 */
	public void clearProcedureContext()
	{
		currProc = null;
	}

	/**
	 * Checks if the String varName is a local variable (in the stack) or whether
	 * it is a global variable
	 * Checks in the parameters, variables, and method name to see if they match given
	 * String (variable) that is being checked
	 * @param varName the String that is being checked to see if it  is a local or global variable
	 * @return true if it is a local variable; false if it is not
	 */
	public boolean isLocalVariable(String varName)
	{
		if (currProc!=null)
		{
			if (varName.equals(currProc.getId()))
				return true;
			List<String> paramNames = currProc.getParams();
			for (String s: paramNames)
			{
				if (varName.equals(s))
					return true;
			}
			for (String s: currProc.getVars())
			{
				if (s.equals(varName))
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Computes the offset (where in the stack the local variable will be located)
	 * specifically whether the string being tested is a parameter, local variable, or a method name
	 * The offsets are in factors of four, and the order in the stack (from bottom to top)
	 * is argument register, parameters, method name, and then local variables at the top
	 * @precondition localVarName is the name of a local variable 
	 * for the procedure currently being compiled
	 * @param localVarName the local variable whose offset is being computed
	 * @return the int offset of the String being checked
	 */
	public int getOffset(String localVarName)
	{
		List<String> parNames = currProc.getParams();
		int numPars = parNames.size();
		int numVars = currProc.getVars().size();
		for (int i = parNames.size()-1; i>=0; i--)
		{
			if (localVarName.equals(parNames.get(i)))
			{
				int offset = 4*(numPars+numVars-i+excessStackHeight); //accounts for method name
				return offset;
			}
		}
		if (localVarName.equals(currProc.getId()))
				return 4*(numVars+excessStackHeight); //should not reach this line
		for (int j = numVars-1; j>=0; j--)
		{
			if (localVarName.equals(currProc.getVars().get(j)))
			{
				int offset = 4*(numVars-1-j+excessStackHeight);
				return offset;
			}
		}
		return 0;//should not reach this line
	}
	
	//prints one line of code to file (with non-labels indented)
	public void emit(String code)
	{
		if (!code.endsWith(":"))
			code = "\t" + code;
		out.println(code);
	}
	
	/**
	 * Code to generate novel id numbers for the end loops or end if labels
	 * Updates the internal counter to give the next number for the endif or endloop 
	 * label (used by while and if classes)
	 * @return the int corresponding to the next id number for the label
	 */
	public int nextLabelID()
	{
		labelId++;
		return labelId;
	}
	
	/**
	 * Pushes the given register to the stack, pushing the stack pointer
	 * down as it does
	 * Excess stack height is increased (for procedure calls)
	 * @param reg the register being pushed onto the stack
	 */
	public void emitPush(String reg)
	{
		emit("subu $sp, $sp, 4");
		emit("sw " + reg +", ($sp)");
		excessStackHeight++;
	}
	
	/**
	 * Pops the argument off the stack and stores it in the passed in register
	 * Moves the stack pointer up
	 * Excess stack height is decreased (for procedure calls)
	 * @param reg the register into which the popped argument is being stored
	 */
	public void emitPop(String reg)
	{
		emit("lw " + reg + ", ($sp)");
		emit("addu $sp, $sp, 4");
		excessStackHeight--;
	}

	//closes the file.  should be called after all calls to emit.
	public void close()
	{
		out.close();
	}
}