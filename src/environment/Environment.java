package environment;

import java.util.*;

import ast.ProcedureDeclaration;
import ast.Statement;
import ast.Variable;

/**
 * Is a framework for environments, which are used to store global/local variables
 * @author AyushAlag
 *
 */
public class Environment 
{
	private Map<String, Integer> variables;
	private Map<String, ProcedureDeclaration> procedures;
	private Environment env;
	
	/**
	 * Assigns the env instance variable to the passed in environment
	 * This is the parent environment, and is null for a global environment
	 * It also instantiates the variables map as a new HashMap (for storing variables) and
	 * the procedures map as a new HashMap (for storing procedures)
	 * @param e is the environment that is passed in and set to env
	 */
	public Environment(Environment e)
	{
		env = e;
		variables = new HashMap<String, Integer>();
		procedures = new HashMap<String, ProcedureDeclaration>();
	}
	
	/**
	 * Getter that returns the parent environment
	 * @return env, the parent environment (may be null if its a global environment)
	 */
	public Environment getParentEnv()
	{
		return env;
	}
	
	/**
	 * Getter that returns the map of variables
	 * @return variables, the HashMap that holds the variables
	 */
	public Map<String, Integer> getVars()
	{
		return variables;
	}
	
	/**
	 * If the variable name is found n the parent environment's variable list,
	 * that variable is modified to the value of "value"; else, if it is not
	 * in the parent environment, the variable is created in the current environment
	 * @param variable the String name of the variable
	 * @param value the int value of the variable
	 */
	public void setVariable(String variable, int value)
	{
		if(!variables.containsKey(variable) && getParentEnv() != null)
		{
			if(env.getVars().containsKey(variable))
				env.declareVariable(variable, value);
			else
				env.setVariable(variable, value);
		}
		else
			variables.put(variable, value);
	}
	
	/**
	 * Does what setVariable used to do: puts the variable in the map
	 * in the current environment
	 * @param variable the String name of the variable
	 * @param value the int value of the variable
	 */
	public void declareVariable(String variable, int value)
	{
		variables.put(variable, value);
	}
	
	/**
	 * Gets the procedure from the global environment
	 * @param id the String of the procedure name
	 * @return the ProcedureDeclaration object of the procedure
	 */
	public ProcedureDeclaration getProcedure(String id) 
	{
		Environment global = this;
		while(global.getParentEnv()!=null)
		{
			global = global.getParentEnv();
		}
		return global.procedures.get(id);
	}
	
	/**
	 * Adds the ProcedureDeclaration (procedure) to the global environment
	 * @param d the ProcedureDeclaration being added to the global environment
	 */
	public void setProcedure(ProcedureDeclaration d) 
	{
		Environment global = this;
		while(global.getParentEnv()!=null)
		{
			global = global.getParentEnv();
		}
		global.procedures.put(d.getId(), d);
	}
	
	/**
	 * Frist checks in the local environment, then the parent environment, for the
	 * variable
	 * @param variable the name of the variable
	 * @return the integer value associated with the variable
	 */
	public int getVariable(String variable)
	{
		if(variables.containsKey(variable))
			return variables.get(variable);
		else
			return getParentEnv().getVariable(variable);
	}
}
