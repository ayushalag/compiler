package parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ast.*;
import ast.Number;
import environment.Environment;
import scanner.*;

/**
 * Creates an abstract tree that can be executed like a compiler by the tester method
 * This tree is lighter and faster than a parse tree
 * Has been updated to the new modified format for global variables, where they are
 * declared before hand to make compiling easier
 * Also takes care of procedures and local variables inside procedures
 * @author AyushAlag
 * @version December 22, 2017
 *
 */
public class Parser 
{
	/**
	 * scanner is the Scanner that performs lexical analysis, currentToken is the token
	 * that we are looking at, and variables is a map that holds the variables created
	 * by the input stream
	 */
	private Scanner scanner;
	private String currentToken;
	
	/**
	 * Sets the scanner variable to the inputted scanner and gets the first token
	 * from the input string/file
	 * @param scan the object of type Scanner passed in that is used for lexical analysis
	 */
	public Parser(Scanner scan)
	{
		scanner = scan;
		try {
			currentToken = scanner.nextToken();
		} catch (ScanErrorException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Uses the next token method from the scanner to get the next token
	 * from the input string/file
	 * @param currToken is checked to see whether it is the same as the currentToken
	 * If it is, then it eats the current token; if not, it throws a ScanErrorException
	 * @throws ScanErrorException thrown if the two strings do not match
	 * @throws IOException thrown in case something goes wrong in the next token method
	 */
	private void eat(String currToken)
	{
		if (currToken.equals(currentToken))
		{
			try {
				currentToken = scanner.nextToken();
			} catch (ScanErrorException | IOException e) {
				e.printStackTrace();
			}
		} else
			try {
				throw new ScanErrorException("did not match; currentToken is " + currentToken + ","
						+ " expected token is " + currToken);
			} catch (ScanErrorException e) {
				e.printStackTrace();
			}
	}
	
	/**
	   * Parses a number and returns it's number object, which contains an int value
	   * for an instance variable
	   * precondition:  current token is an integer
	   * postcondition: number token has been eaten
	   * @return the value of the parsed integer
	   * @throws IOException 
	   * @throws ScanErrorException 
	   */
   private Number parseNumber() throws ScanErrorException
   {
	   Number num = new Number(Integer.parseInt(currentToken));
	   eat(currentToken);
	   return num;
   }
   
   /**
    * Parses the elements that are part of the program, consisting of a list (possibly 0)
    * of variables, a list (possibly 0) of procedures (which may have local variables),
    * followed by a final statement that is to be executed
    * @return the Program variable that holds all of the procedures and the final
    * statement
    * @throws ScanErrorException because it uses the eat method, which may
    * encounter a different char than is expected
    * @throws IOException if an error occurs when parsing
    */
   public Program parseProgram() throws ScanErrorException, IOException
   {
	   List<ProcedureDeclaration> procedures = new ArrayList<ProcedureDeclaration>();
	   List<String> variables = new ArrayList<String>();
	   if (currentToken.equals("VAR"))
	   {
		   eat(currentToken);
		   variables.add(currentToken);
		   eat(currentToken);
		   while(currentToken.equals(","))
		   {
			   eat(currentToken);
			   variables.add(currentToken);
			   eat(currentToken);
		   }
		   eat(";");
	   }
	   while (currentToken.equals("PROCEDURE"))
	   {
		   eat("PROCEDURE");
		   String id = currentToken;
		   eat(currentToken);
		   eat("(");
		   List<String> params = new ArrayList<String>();
		   while (!currentToken.equals(")"))
		   {
			   params.add(currentToken);
			   eat(currentToken);
			   if (currentToken.equals(","))
			   {
				   eat(currentToken);
			   }
		   }
		   eat(")");
		   eat(";");
		   List<String> vars = new ArrayList<String>();
		   if (currentToken.equals("VAR"))
		   {
			   eat(currentToken);
			   vars.add(currentToken);
			   eat(currentToken);
			   while(currentToken.equals(","))
			   {
				   eat(currentToken);
				   vars.add(currentToken);
				   eat(currentToken);
			   }
			   eat(";");
		   }
		   Statement elseStatement = parseStatement();
		   procedures.add(new ProcedureDeclaration(id,elseStatement, params, vars));
	   }
	   Statement endstmt = parseStatement();
	   return new Program(variables, procedures, endstmt);
   }
   
   /**
    * Parses a statement (set of instructions)
    * Checks WRITELN, BEGIN, and variable assignments as starters of expressions; it then
    * calls the parseExpression method and takes care of any leftover paranthesis and semicolons
    * It also takes care of 
    * @throws ScanErrorException
    * @throws IOException 
    */
   public Statement parseStatement() throws ScanErrorException, IOException
   {
	   if (currentToken.equals("WRITELN"))
	   {
		   eat(currentToken);
		   eat("(");
		   Expression exp = parseExpression();
		   eat(")");
		   eat(";");
		   return new Writeln(exp);
	   }
	   else if (currentToken.equals("READLN"))
	   {
		   eat(currentToken);
		   eat("(");
		   String varName = currentToken;
		   eat(currentToken);
		   System.out.println("Input a number or expression");
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   String varValue = br.readLine();
		   Number n = new Number(Integer.parseInt(varValue));
		   eat(")");
		   eat(";");
		   return new Assignment(varName, n);
	   }
	   else if (currentToken.equals("BEGIN"))
	   {
		  eat(currentToken);
		  List<Statement> smts = new ArrayList<Statement>();
		  while (!currentToken.equals("END"))
		  {
			  smts.add(parseStatement());
		  }
		  eat("END");
		  eat(";");
		  return new Block(smts);
	   }
	   else if (currentToken.equals("IF"))
	   {
		   eat(currentToken);
		   Expression exp1 = parseExpression();
		   String relop = currentToken;
		   eat(currentToken);
		   Expression exp2 = parseExpression();
		   Condition cond = new Condition(exp1,relop,exp2);
		   eat("THEN");
		   Statement stmt = parseStatement();
		   if (currentToken.equals("ELSE"))
		   {
			   eat("ELSE");
			   Statement elsestmt = parseStatement();
			   return new ElseIf(cond, stmt, elsestmt);
		   }
		   return new If(cond,stmt);
	   }
	   else if (currentToken.equals("WHILE"))
	   {
		   eat(currentToken);
		   Expression exp1 = parseExpression();
		   String relop = currentToken;
		   eat(currentToken);
		   Expression exp2 = parseExpression();
		   Condition cond = new Condition(exp1,relop,exp2);
		   eat("DO");
		   Statement stmt = parseStatement();
		   return new While(cond,stmt);
	   }
	   else if (currentToken.equals("FOR"))
	   {
		   eat(currentToken);
		   String start = currentToken;
		   eat(currentToken);
		   eat(":=");
		   Expression exp = parseExpression();
		   eat ("TO");
		   Expression exp2 = parseExpression();
		   eat("DO");
		   Statement stmt = parseStatement();
		   return new For(start, exp, exp2, stmt);
	   }
	   else
	   {
		   //assuming it is a variable
		   String varName = currentToken;
		   eat(currentToken);
		   eat(":=");
		   Expression num = parseExpression();
		   eat(";");
		   return new Assignment(varName, num);
	   }
   }
   
   /**
    * Parses a factor following the rules of the grammar
    * If the current token is a paranthesis, it eats it and parses the expression inside
    * using the parse expression method. It then takes care of the closing paranthesis
    * If the current token is a - sign, it returns a negative of the following number
    * 
    * Otherwise, if the current token is a String (id) it is checked to see
    * whether if it is followed by paranthesis or not (to see whether it is a procedure). 
    * If it is a procedure, a new Procedure Call is created using the name of the procedure
    * that is called and the actual parameters (expressions) of the procedure.
    * If it is not a procedure, it is treated as a variable and a Variable
    * object is returned.
    * 
    * In any other case, the token being looked at will be a number,
    * and this will be parsed by the parseNumber method.
    * @return the int of the factor computed by this method
    * @throws ScanErrorException because eat is called (in case tokens do not match)
    */
   public Expression parseFactor() throws ScanErrorException
   {
	   if (currentToken.equals("("))
	   {
		  eat(currentToken);
		  Expression e = parseExpression();
		  eat(")");
		  return e;
	   }
	   else if (currentToken.equals("-"))
	   {
		   eat(currentToken);
		   return new BinOp("-", new Number(0), parseFactor());
	   }
	   else if (currentToken.toLowerCase().substring(0).compareTo("a")>=0 &&
			   currentToken.toLowerCase().substring(0).compareTo("z")<=0)
	   {
		   String var = (currentToken);
		   eat(currentToken);
		   if(currentToken.equals("("))
		   {
			   eat("(");
			   List<Expression> params = new ArrayList<Expression>();
			   while (!currentToken.equals(")"))
			   {
				   params.add(parseExpression());
				   if (currentToken.equals(","))
				   {
					   eat(currentToken);
				   }
			   }
			   eat(")");
			   ProcedureCall pCall = new ProcedureCall(var, params);
			   return pCall;
		   }
		   return new Variable(var);
	   }
	   else
	   {
		   return parseNumber();
	   }
   }
   
   /**
    * Parses a term, defined to be a factor or factors multiplied or divided 
    * @return the integer computed by in this term
    * @throws ScanErrorException if eat has an exception (characters do not match)
    */
   public Expression parseTerm() throws ScanErrorException
   {
	   Expression sum = parseFactor();
	   while(currentToken.equals("*") || currentToken.equals("/") || currentToken.equals("mod"))
	   {
		   String op = currentToken;
		   eat(currentToken);
		   sum = new BinOp(op, sum, parseFactor());
	   }
	   return sum;
   }
   
   /**
    * Parses an expression, which is defined as a term, an expression + a term, 
    * or an expression - a term
    * @return the integer computed in the expression
    * @throws ScanErrorException if eat has an exception (characters do not match)
    */
   public Expression parseExpression() throws ScanErrorException
   {
	   Expression sum = parseTerm();
	   while (currentToken.equals("+")||currentToken.equals("-"))
	   {
		   String op = currentToken;
		   eat(currentToken);
		   sum = new BinOp(op, sum, parseTerm());
	   }
	   return sum;
   }
}
