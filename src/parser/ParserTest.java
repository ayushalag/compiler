package parser;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import ast.Emitter;
import ast.Program;
import environment.Environment;
import scanner.ScanErrorException;
import scanner.Scanner;

/**
 * Tests the parser using an inputted file to see if it converts it to the outputted
 * file correctly
 * Takes care completely of while loops, if conditions, and basic functionality
 * Also updated to test for procedure calls (including parameters, local variables, and return values)
 * @author AyushAlag
 * @version December 22, 2017
 *
 */
public class ParserTest 
{
	public static void main(String [] args) throws ScanErrorException, IOException
	{
		InputStream in = new DataInputStream(new FileInputStream(new File(""
			+ "/Users/AyushAlag/Documents/CodeGenTest.txt")));
		Scanner scan = new Scanner (in);
		Parser parse = new Parser (scan);
		Environment env = new Environment(null);
		Emitter e = new Emitter("/Users/AyushAlag/Documents/Output.asm");
		parse.parseProgram().compile(e);
	}
		
}
