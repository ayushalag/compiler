package scanner;

import java.io.*;

/**
 * Scanner is a simple scanner for Compilers and Interpreters (2014-2015) lab exercise 1
 * @author Ayush Alag
 * @version September 13, 2017
 *  
 * Usage:
 * This class can read in a text and split it up into different tokens, whether they be identifiers,
 * operands, or numbers. It also removes white space and comments.
 *
 */
public class Scanner
{
    private PushbackReader in;
    private char currentChar;
    private boolean eof;
    
    /**
     * Scanner constructor for construction of a scanner that 
     * uses an InputStream object for input.  
     * Usage: 
     * FileInputStream inStream = new FileInputStream(new File(<file name>);
     * Scanner lex = new Scanner(inStream);
     * @param inStream the input stream to use
     */
    public Scanner(InputStream inStream)
    {
        in = new PushbackReader((new InputStreamReader(inStream)));
        eof = false;
        getNextChar();
    }
    /**
     * Scanner constructor for constructing a scanner that 
     * scans a given input string.  It sets the end-of-file flag an then reads
     * the first character of the input string into the instance field currentChar.
     * Usage: Scanner lex = new Scanner(input_string);
     * @param inString the string to scan
     */
    public Scanner(String inString)
    {
        in = new PushbackReader((new StringReader(inString)));
        eof = false;
        getNextChar();
    }
    
    /**
     * Method: getNextChar sets current char to the next character in the input stream
     * It also checks whether it is the end of the file or not
     */
    private void getNextChar()
    {
    	try
    	{
    		int num = in.read();
    		if (num!=-1)
    			currentChar = (char) num;
    		else
    			eof = true;
    	}
    	catch (IOException e)
    	{
    		System.out.println("IOException");
    	}
    	finally
    	{
    		
    	}
    }
    
    /**
     * Method: eat checks if the expected value is currentChar and then calls the
     * getNextChar method
     */
    private void eat(char expected) throws ScanErrorException
    {
        if (expected==currentChar)
        	getNextChar();
        else
        	throw new ScanErrorException("Illegal Character - Expected " 
        			+ expected + "and found " + currentChar);
    }
    
    /**
     * Checks whether the given character is a digit
     * @param c the character being checked to see if it is a digit
     * @return true if it is a digit, false if otherwise
     */
    public static boolean isDigit(char c)
    {
    	return (c >= '0' && c<='9');
    }
    
    /**
     * Checks whether the given character is a letter
     * @param c the character being checked to see if it is a letter
     * @return true if it is a letter, false if otherwise
     */
    public static boolean isLetter(char c)
    {
    	return ((c >='A' && c<='Z')|| (c>='a' && c<='z'));
    }
    
    /**
     * Checks whether the given character is white space or a new line or new tab
     * @param c the character being checked
     * @return true if it is white space, false otherwise
     */
    public static boolean isWhiteSpace(char c)
    {
    	return (c == ' ' || c == '\t' || c == '\r' || c == '\n');
    }
    
    public static boolean isOperand(char c)
    {
    	return (c == '=' || c == '+' ||c == '-' ||c == '*' ||
    			c == '%' || c == ':' || c== '/'
    			|| c == '('|| c == ')' || c == '<'|| c == '>'|| c==',' || c == '.');
    }
    
    public static boolean isDelimiter(char c)
    {
    	return c ==';';
    }
    
    /**
     * Method: hasNext
     * @return true if there are more characters in the file, false if the end of the file has been reached
     */
    public boolean hasNext()
    {
       return !eof;
    }
    
    /**
     * Eats the text until it reaches the end of a number
     * @return the number in the form of a string
     * @throws ScanErrorException because of eat; if currentchar does not match expected it will
     * throw this error
     */
    private String scanNumber() throws ScanErrorException
    {
    	String number = "";
    	while (hasNext()&&isDigit(currentChar))
    	{
    		number+= currentChar;
    		eat(currentChar);
    	}
    	return number;
    }
    
    /**
     * Eats the text until it reaches the end of an identifier, which is defined as a letter followed by
     * a letter or digit
     * @return the String form of the identifier
     * @throws ScanErrorException because of eat; if currentchar does not match expected
     * it will throw this error
     */
    private String scanIdentifier() throws ScanErrorException
    {
    	String letter = "";
    	while (hasNext() && (isLetter(currentChar) || isDigit(currentChar)))
    	{
    		letter+= currentChar;
    		eat(currentChar);
    	}
    	return letter;
    }
    
    /**
     * Eats the text until it reaches the end of an operand
     * Also handles compound operands, such as <>, <=, and >=
     * @return the String form of the operand
     * @throws ScanErrorException because of eat; if currentchar does not match expected it will throw
     * this error
     * @throws IOException 
     */
    private String scanOperand() throws ScanErrorException, IOException
    {
    	if (currentChar == ':')
    	{
    		eat(':');
    		eat('=');
    		return ":=";
    	}
    	else if (currentChar == '<')
    	{
    		eat(currentChar);
    		if (currentChar == '=')
    		{
    			eat(currentChar);
    			return "<=";
    		}
    		else if (currentChar == '>')
    		{
    			eat(currentChar);
    			return "<>";
    		}
    		else
    		{
    			return "<";
    		}
    	}
    	else if (currentChar == '>')
    	{
    		eat(currentChar);
    		if (currentChar == '=')
    		{
    			return ">=";
    		}
    		else
    		{
    			return ">";
    		}
    	}
    	else
    	{
    		String temp = "" + currentChar;
    		eat(currentChar);
    		return temp;
    	}
    }
    
    /**
     * Eats the semicolon and returns it
     * @return the semicolon
     * @throws ScanErrorException if eat does not work as expected
     */
    private String scanDelimiter() throws ScanErrorException
    {
    	eat(currentChar);
    	return ";";
    }
    
    /**
     * Takes care of in-line comments and block comments, skipping the characters inside of them
     * and then returning the next token
     * @return the next token after the comments
     * @throws ScanErrorException if eat doesn't work as expected
     * @throws IOException for unread
     */
    public String inLineCommentHelper() throws ScanErrorException, IOException
    {
    	eat(currentChar);
		if (currentChar == '/')
		{
			while (currentChar != '\n')
			{
				eat(currentChar);
			}
			return nextToken();
		}
		else if (currentChar == '*')
		{
			char temp = currentChar;
			eat(currentChar);
			while (!(temp == '*' && currentChar == '/'))
			{
				temp = currentChar;
				eat(currentChar);
			}
			return nextToken();
		}
		else
		{
			in.unread(currentChar);
			return nextToken();
		}
		
    }
    
    /**
     * Method: nextToken checks the beginning character and uses that to either scan a number,
     * identifier, or operand. It then returns that token as a string. It also checks for comments
     * @return the String form of the next token found in the text file
     * @throws IOException because it calls inLineCommentHelper(), which calls unread()
     * This has the possibility of throwing an IOException
     */
    
    public String nextToken() throws ScanErrorException, IOException
    {
    	while (hasNext()&&isWhiteSpace(currentChar) )
    	{
    		eat(currentChar);
    	}
    	if (!hasNext())
    		return "END";
    	if (isDigit(currentChar))
    	{
    		return scanNumber();
    	}
    	if (isLetter(currentChar))
    		return scanIdentifier();
    	if (isOperand(currentChar))
    	{
    		return scanOperand();
    	}
    	if (isDelimiter(currentChar))
    		return scanDelimiter();
    	if (currentChar == '/')
    	{
    		return inLineCommentHelper();
    	}
    	else
    		throw new ScanErrorException("unknown char" + currentChar);
    }
}