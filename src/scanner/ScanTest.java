package scanner;

import java.io.*;
/**
 * Is a tester for the scanner class. It reads in a file and then prints out all of the tokens from 
 * that file.
 * @author AyushAlag
 * @version September 8, 2017
 *
 */
public class ScanTest 
{
	public static void main(String [] args) throws ScanErrorException, IOException
	{
		File file = new File("/Users/AyushAlag/Downloads/ScannerTest.txt");
		InputStream stream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
		File file2 = new File("/Users/AyushAlag/Downloads/scannerTestAdvanced.txt");
		InputStream stream2 = new DataInputStream(new BufferedInputStream(new FileInputStream(file2)));
		Scanner scan = new Scanner(stream);
		while(scan.hasNext())
		{
			System.out.println(scan.nextToken());
		}
		Scanner scan2 = new Scanner(stream2);
		while(scan2.hasNext())
		{
			System.out.println(scan2.nextToken());
		}
	}
}
