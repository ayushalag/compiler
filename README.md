# Compiler Project
Final project from the Honors Advanced Topics: Compilers and Interpreters class that builds an end-to-end compiler. Compiles a PASCAL-like language and outputs it as MIPS Assembly language code.
The scanner package is in charge of reading the input code, which is then parsed by the parser. 
The ast package uses environment (which stores the local and global variables) to build an abstract syntax tree. It can be configured to output the MIPS code (acting
as a compiler) or evaluate the abstract syntax tree (acting as an interpreter).

---

## Key Classes

### ast/

#### Assignment
Is an assignment operation, containing a variable name and an expression value
  Creates a new variable in the environment with the given variable name and value
  Can be compiled to store the variable in memory

#### BinOp
 Is an binary operator, containing two expressions and an operator
  Evaluates the two expressions using the operator

#### Block
 Framework for a block of statements; each is stored in a list,
  and when the method is called to execute the block, each statement is executed

#### Condition
 Framework for the condition function, which can evaluate (using a relative operator)
  if a condition is true (using both expressions and evaluating them with the relative operator)

#### ElseIf
Used if an else-if statement is found in the text
  Checks whether the first condition is true; if it is, then it evaluates the first statement;
  else, it evaluates the last statement.

#### Emitter
 Can emit the inputted text to an external file
  Also modified to push and pop onto the stack

#### Expression
 Abstract framework for subclasses of Expression
 Each expression object/class can evaluate itself using an evironment
 It can also compile itself in MIPS code

#### ProcedureCall
Framework for when a procedure is called; holds the name of the procedure
  and the values (Expressions) of the inputted parameters. When evaluated,
  the formal and actual parameters are equated and stored as variables and
  the statement associated with the procedure is called
  Takes care of global and local environments

#### Program
 Class that holds all of the procedures in the program as well as the final
  statement that is to be executed
  Also, when executed, adds all of the procedures to the global environment, where they
  are stored
  Can be compiled to produce MIPS code that can be used in QtSpim

#### Statement
 Abstract framework for Statement classes
 All statements can execute themselves using an environment
 All statements can compile themselves using an emitter

#### While
Is used to handle while loops
  Continually executes the statement while the condition is true
  Can also be compiled to MIPS code

#### Writeln
 Handles the WRITELN block by printing out the value of the 
  expression contained inside the block
  Can also be compiled to MIPS code
 
### env/Environment
 Is a framework for environments, which are used to store global/local variables

### parser/Parser
 Creates an abstract tree that can be executed like a compiler by the tester method
  This tree is lighter and faster than a parse tree
  Has been updated to the new modified format for global variables, where they are
  declared before hand to make compiling easier
  Also takes care of procedures and local variables inside procedures

### scanner/Scanner
 Scanner is a simple scanner for Compilers and Interpreters lab exercise 1
  